﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Garbage_Collection_Destructor
{
    class Program
    {
        static void Main(string[] args)
        {
            new Student().StudentMethod();
        }
    }

    public class Student
    {
        public Student()
        {
            Console.WriteLine("constructor call");
        }

        public void StudentMethod()
        {
            Console.WriteLine("Student test method");
        }

        ~Student()
        {
            Console.WriteLine("destructor call");
        }
    }
}
